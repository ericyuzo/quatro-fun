﻿using System;

namespace QuatroFun.Extensions
{
    public static class RandomExtensions
    {
        /// <summary>
        /// Returns a boolean <c>true</c> value with specified probability 
        /// (a Bernoulli random variable).
        /// </summary>
        public static bool Flip(this Random rnd, double probability)
        {
            return rnd.NextDouble() < probability;
        }
    }
}
