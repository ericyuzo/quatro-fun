﻿using System;
using QuatroFun.Ga;

namespace QuatroFun
{
    class Program
    {
        static void Main(string[] args)
        {
            int stringLength = 36;
            FitnessFunction fitnessFunction = new BlackBoxFitnessFunction();

            BitStringGa ga = new BitStringGa(stringLength, fitnessFunction);
            ga.Parameters.PopulationSize = 50;
            ga.Parameters.MaxGenerations = 50;
            ga.Parameters.CrossoverProbability = 0.6; // 60%
            ga.Parameters.MutationProbability = 0.0333; // 3.33%
            ga.Selection = new TournamentSelection(3);
            ga.Crossover = new OnePointCrossover();
            ga.Mutation = new BitFlipMutation();
            ga.run();

            Console.ReadKey();
        }
    }
}
