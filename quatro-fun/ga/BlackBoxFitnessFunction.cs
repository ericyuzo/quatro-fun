﻿namespace QuatroFun.Ga
{
    /// <summary>
    /// Fitness function for black box parameters optimization problem defined by Fernando Von Zuben.
    /// [source (in portuguese): ftp://vm1-dca.fee.unicamp.br/pub/docs/gudwin/ea072/tutorialEC.pdf]
    /// </summary>
    public class BlackBoxFitnessFunction : FitnessFunction
    {
        /// <summary>
        /// Sets the fitness value for specified <c>individual</c>.
        /// </summary>
        /// <remarks>
        /// Fitness function (given by the author):
        /// 9 + bit1  * bit4  - bit22 * bit13 + bit23 * bit3  - bit20 * bit9
        ///   + bit35 * bit14 - bit10 * bit25 + bit15 * bit16 + bit2  * bit32
        ///   + bit27 * bit18 + bit11 * bit33 - bit30 * bit31 - bit21 * bit24
        ///   + bit34 * bit26 - bit28 * bit6  + bit7  * bit12 - bit5  * bit8
        ///   + bit17 * bit19 - bit0  * bit29 + bit22 * bit3  + bit20 * bit14
        ///   + bit25 * bit15 + bit30 * bit11 + bit24 * bit18 + bit6  * bit7
        ///   + bit8  * bit17 + bit0  * bit32;
        /// 
        /// Optimal solution: fitness = 27
        /// </remarks>
        public void Evaluate(Individual individual)
        {
            int[] b = ConvertToIntArray(individual);
            double fitness = 9 + b[1]  * b[4]  - b[22] * b[13] + b[23] * b[3]  - b[20] * b[9]
                               + b[35] * b[14] - b[10] * b[25] + b[15] * b[16] + b[2]  * b[32]
                               + b[27] * b[18] + b[11] * b[33] - b[30] * b[31] - b[21] * b[24]
                               + b[34] * b[26] - b[28] * b[6]  + b[7]  * b[12] - b[5]  * b[8]
                               + b[17] * b[19] - b[0]  * b[29] + b[22] * b[3]  + b[20] * b[14]
                               + b[25] * b[15] + b[30] * b[11] + b[24] * b[18] + b[6]  * b[7]
                               + b[8]  * b[17] + b[0]  * b[32];
            individual.Fitness = fitness;
        }

        private int[] ConvertToIntArray(Individual individual)
        {
            int[] chrom = new int[individual.Length];
            for (int i = 0; i < chrom.Length; i++)
                chrom[i] = individual[i].IntValue;

            return chrom;
        }
    }
}
