﻿namespace QuatroFun.Ga
{
    public interface FitnessFunction
    {
        /// <summary>
        /// Sets the fitness value for specified <c>individual</c>.
        /// </summary>
        void Evaluate(Individual individual);
    }
}
