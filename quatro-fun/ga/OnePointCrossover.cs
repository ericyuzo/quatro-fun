﻿using QuatroFun.Extensions;
using System;

namespace QuatroFun.Ga
{
    public class OnePointCrossover : CrossoverOperator
    {
        private Random rnd = new Random();

        public Offsprings Breed(Individual parent1, Individual parent2, double probability)
        {
            if (parent1.Length != parent2.Length)
                throw new ArgumentException("parents must be same length.");

            Individual offspring1 = (Individual)parent1.Clone();
            Individual offspring2 = (Individual)parent2.Clone();

            if (rnd.Flip(probability))
            {
                int crossPoint = rnd.Next(1, parent1.Length - 1);
                for (int i = 0; i < crossPoint; i++)
                {
                    offspring1[i] = parent2[i];
                    offspring2[i] = parent1[i];
                }
            }

            return new Offsprings(offspring1, offspring2);
        }
    }
}
