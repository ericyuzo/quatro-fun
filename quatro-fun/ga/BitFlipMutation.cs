﻿using QuatroFun.Extensions;
using System;

namespace QuatroFun.Ga
{
    public class BitFlipMutation : MutationOperator
    {
        private Random rnd = new Random();

        public void Apply(Individual individual, double probability)
        {
            for (int i = 0; i < individual.Length; i++)
                if (rnd.Flip(probability))
                    individual[i] = individual[i].Flip();
        }
    }
}
