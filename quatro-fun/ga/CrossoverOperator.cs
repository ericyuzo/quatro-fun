﻿namespace QuatroFun.Ga
{
    public interface CrossoverOperator
    {
        Offsprings Breed(Individual parent1, Individual parent2, double probability);
    }
}
