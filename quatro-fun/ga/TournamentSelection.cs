﻿using System;
using System.Linq;

namespace QuatroFun.Ga
{
    public class TournamentSelection : SelectionOperator
    {
        private int size;

        public int Size
        {
            get { return size; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("Size must be a positive number");
                size = value;
            }
        }

        private Random rnd = new Random();

        public TournamentSelection(int size)
        {
            this.Size = size;
        }

        public Individual Select(Individual[] population, Statistics stats)
        {
            Individual[] competitors = new Individual[Size];
            for (int i = 0; i < Size; i++)
                competitors[i] = population[rnd.Next(population.Length)];
            Individual winner = competitors.OrderByDescending(c => c.Fitness).First();
            return winner;
        }
    }
}
