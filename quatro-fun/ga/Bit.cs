﻿namespace QuatroFun.Ga
{
    public class Bit
    {
        public static readonly Bit ZERO = new Bit(false);
        public static readonly Bit ONE = new Bit(true);

        public static Bit FromBool(bool value)
        {
            return value ? ONE : ZERO;
        }

        public bool Value { get; private set; }

        private Bit(bool value)
        {
            this.Value = value;
        }

        public int IntValue
        {
            get { return Value ? 1 : 0; }
        }

        public Bit Flip()
        {
            return Value ? ZERO : ONE;
        }

        public override string ToString()
        {
            return Value ? "1" : "0";
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            Bit b = obj as Bit;
            if ((object)b == null)
                return false;

            return this.Value == b.Value;
        }

        public override int GetHashCode()
        {
            return IntValue;
        }
    }
}
