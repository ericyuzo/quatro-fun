﻿namespace QuatroFun.Ga
{
    public class Offsprings
    {
        public Individual First { get; private set; }
        public Individual Second { get; private set; }

        public Offsprings(Individual first, Individual second)
        {
            this.First = first;
            this.Second = second;
        }
    }
}
