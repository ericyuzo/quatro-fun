﻿using System;
using System.Linq;
using System.Text;

namespace QuatroFun.Ga
{
    public class Individual : ICloneable
    {
        private Bit[] chromosome;

        public int Length { get; private set; }

        public double Fitness { get; set; }

        public Individual(int length)
        {
            if (length <= 0)
                throw new ArgumentOutOfRangeException("Length must be a positive number.");
            this.Length = length;

            chromosome = new Bit[this.Length];
            for (int i = 0; i < this.Length; i++)
                chromosome[i] = Bit.ZERO;
        }

        public Individual(Bit[] bitString)
        {
            if (bitString == null || bitString.Length == 0)
                throw new ArgumentException("bitString cannot be null or empty.");

            chromosome = new Bit[bitString.Length];
            Array.Copy(bitString, chromosome, bitString.Length);

            this.Length = chromosome.Length;
        }

        public Bit this[int i]
        {
            get { return chromosome[i]; }
            set { chromosome[i] = value; }
        }

        public override string ToString()
        {
            StringBuilder buffer = new StringBuilder();
            foreach (Bit b in chromosome.Reverse())
                buffer.Append(b);

            return buffer.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            Individual individual = obj as Individual;
            if ((object)individual == null)
                return false;

            return Fitness == individual.Fitness && chromosome.SequenceEqual(individual.chromosome);
        }

        public override int GetHashCode()
        {
            int result = 17;
            result = 31 * result + Fitness.GetHashCode();
            result = 31 * result + ToString().GetHashCode();
            return result;
        }

        public object Clone()
        {
            Individual copy = new Individual(chromosome);
            copy.Fitness = this.Fitness;
            return copy;
        }
    }
}
