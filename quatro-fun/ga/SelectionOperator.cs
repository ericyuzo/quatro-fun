﻿namespace QuatroFun.Ga
{
    public interface SelectionOperator
    {
        Individual Select(Individual[] population, Statistics stats);
    }
}
