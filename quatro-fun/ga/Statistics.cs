﻿using System.Linq;

namespace QuatroFun.Ga
{
    public class Statistics
    {
        public double MinFitness { get; private set; }
        public double MaxFitness { get; private set; }
        public double AvgFitness { get; private set; }
        public double SumFitness { get; private set; }

        public Statistics(Individual[] population)
        {
            MinFitness = population.Min(individual => individual.Fitness);
            MaxFitness = population.Max(individual => individual.Fitness);
            SumFitness = population.Sum(individual => individual.Fitness);
            AvgFitness = SumFitness / population.Length;
        }
    }
}
