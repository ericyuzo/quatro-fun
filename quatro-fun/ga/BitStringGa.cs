﻿using QuatroFun.Extensions;
using System;

namespace QuatroFun.Ga
{
    public class BitStringGa
    {
        // Problem definition attributes
        private int stringLength;
        private FitnessFunction fitnessFunction;

        // GA parameters
        public GaParameters Parameters { get; set; }

        // Genetic operators
        public SelectionOperator Selection { get; set; }
        public CrossoverOperator Crossover { get; set; }
        public MutationOperator Mutation { get; set; }

        // Variables
        private Individual[] population;

        private int generation;

        private Statistics stats;

        private Random rnd = new Random();

        public BitStringGa(int stringLength, FitnessFunction fitnessFunction)
            : this(stringLength, fitnessFunction, new GaParameters(30, 10, 0.6, 0.0333)) { }

        public BitStringGa(int stringLength, FitnessFunction fitnessFunction, GaParameters parameters)
        {
            if (stringLength <= 0)
                throw new ArgumentOutOfRangeException("stringLength must be a positive number");
            this.stringLength = stringLength;
            if (fitnessFunction == null)
                throw new ArgumentNullException("fitnessFunction cannot be null");
            this.fitnessFunction = fitnessFunction;

            this.Parameters = parameters;

            this.Selection = new RouletteWheelSelection();
            this.Crossover = new OnePointCrossover();
            this.Mutation = new BitFlipMutation();
        }

        // Aliases
        private int popSize { get { return Parameters.PopulationSize; } }
        private int maxGen { get { return Parameters.MaxGenerations; } }
        private double cp { get { return Parameters.CrossoverProbability; } }
        private double mp { get { return Parameters.MutationProbability; } }

        // Algorithm
        public void run()
        {
            generation = 1;
            initialize();
            evaluate();
            print();
            while (!canTerminate())
            {
                generation++;
                reproduce();
                evaluate();
                print();
            }
        }

        // Generate a random initial population
        // The genetic representation of a candidate solution is an array of bits
        private void initialize()
        {
            population = new Individual[popSize];

            for (int i = 0; i < popSize; i++)
            {
                // generate a random individual
                Individual individual = new Individual(this.stringLength);
                for (int j = 0; j < stringLength; j++)
                    individual[j] = Bit.FromBool(rnd.Flip(0.5));

                population[i] = individual;
            }
        }

        // Evaluate the fitness of current generation individuals
        // Compute some statistics from population
        private void evaluate()
        {
            foreach (var individual in population)
                fitnessFunction.Evaluate(individual);

            stats = new Statistics(population);
        }

        // Creates a new generation
        private void reproduce()
        {
            Individual[] newGeneration = new Individual[popSize];

            for (int i = 0; i < popSize; i += 2)
            {
                Individual mate1 = (Individual)Selection.Select(population, stats);
                Individual mate2 = (Individual)Selection.Select(population, stats);

                Offsprings offsprings = Crossover.Breed(mate1, mate2, cp);
                Mutation.Apply(offsprings.First, mp);
                Mutation.Apply(offsprings.Second, mp);

                newGeneration[i] = offsprings.First;
                newGeneration[i + 1] = offsprings.Second;
            }

            population = newGeneration;
        }

        // termination condition: Fixed number of generations reached
        private bool canTerminate()
        {
            return generation == maxGen;
        }

        // print the genetic representation and the fitness of every individuals from current generation
        private void print()
        {
            Console.WriteLine("Generation #" + generation);
            for (int i = 0; i < popSize; i++)
                Console.WriteLine(population[i] + " --> " + population[i].Fitness);
            Console.WriteLine("Minimum Fitness = " + stats.MinFitness);
            Console.WriteLine("Average Fitness = " + stats.AvgFitness);
            Console.WriteLine("Maximum Fitness = " + stats.MaxFitness);
        }
    }
}
