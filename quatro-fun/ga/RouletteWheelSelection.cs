﻿using System;

namespace QuatroFun.Ga
{
    public class RouletteWheelSelection : SelectionOperator
    {
        private Random rnd = new Random();

        public Individual Select(Individual[] population, Statistics stats)
        {
            double pocket = rnd.NextDouble() * stats.SumFitness;
            double partialSum = 0;
            Individual selected = null;
            for (int i = 0; i < population.Length; i++)
            {
                partialSum += population[i].Fitness;
                if (partialSum >= pocket)
                {
                    selected = population[i];
                    break;
                }
            }
            return selected;
        }
    }
}
