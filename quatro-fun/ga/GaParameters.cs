﻿using System;

namespace QuatroFun.Ga
{
    public class GaParameters
    {
        private int populationSize;
        private int maxGenerations;
        private double crossoverProbability;
        private double mutationProbability;

        public GaParameters(int populationSize, int maxGenerations,
            double crossoverProbability, double mutationProbability)
        {
            this.PopulationSize = populationSize;
            this.MaxGenerations = maxGenerations;
            this.CrossoverProbability = crossoverProbability;
            this.MutationProbability = mutationProbability;
        }

        public int PopulationSize
        {
            get { return populationSize; }
            set
            {
                if (value <= 0 || value % 2 == 1)
                    throw new ArgumentOutOfRangeException("PopulationSize must be a positive even number");
                populationSize = value;
            }
        }
        public int MaxGenerations
        {
            get { return maxGenerations; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("MaxGenerations must be a positive number");
                maxGenerations = value;
            }
        }
        public double CrossoverProbability
        {
            get { return crossoverProbability; }
            set
            {
                if (value < 0.0 || value > 1.0)
                    throw new ArgumentOutOfRangeException("CrossoverProbability must be a number between 0 and 1");
                crossoverProbability = value;
            }
        }
        public double MutationProbability
        {
            get { return mutationProbability; }
            set
            {
                if (value < 0.0 || value > 1.0)
                    throw new ArgumentOutOfRangeException("MutationProbability must be a number between 0 and 1");
                mutationProbability = value;
            }
        }
    }
}
