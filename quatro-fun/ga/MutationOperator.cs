﻿namespace QuatroFun.Ga
{
    public interface MutationOperator
    {
        void Apply(Individual individual, double probability);
    }
}
